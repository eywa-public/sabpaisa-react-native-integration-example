//
//  SabpaisaModule.m
//  AwesomeProject2
//
//  Created by Lokesh Deshmukh on 10/03/23.
//

#import <Foundation/Foundation.h>
#import "AwesomeProject2-Bridging-Header.h"
#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(SabPaisaSDK, NSObject)
RCT_EXTERN_METHOD(openSabpaisaSDK:(NSArray)params callback:(RCTResponseSenderBlock)callback);

@end


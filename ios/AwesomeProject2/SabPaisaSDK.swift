//
//  Sabpaisa.swift
//  AwesomeProject2
//
//  Created by Lokesh Deshmukh on 10/03/23.
//

import SabPaisa_IOS_Sdk
import Foundation
import UIKit


@objc(SabPaisaSDK)
public class SabPaisaSDK: UIViewController {
  
  
  
  var initUrl="https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
  var baseUrl="https://sdkstaging.sabpaisa.in"
  var transactionEnqUrl="https://stage-txnenquiry.sabpaisa.in"
  
  @objc public func openSabpaisaSDK(_ params:NSArray,callback: @escaping  RCTResponseSenderBlock) {
    
    let bundle = Bundle(identifier: "com.eywa.ios.SabPaisa-IOS-Sdk")
    let storyboard = UIStoryboard(name: "Storyboard", bundle: bundle)
    
    
    let amount = Float(params[0] as! String)!
    let firstName = (params[1] as! String)
    let lastName = (params[2] as! String)
    let mobileNumber = (params[3] as! String)
    let emailId = (params[4] as! String)
    
    
    let secKey="kaY9AIhuJZNvKGp2"
    let secInivialVector="YN2v8qQcU3rGfA1y"
    let transUserName="rajiv.moti_336"
    let transUserPassword="RIADA_SP336"
    let clientCode="TM001"
    
    DispatchQueue.main.async{
      
      let vc = storyboard.instantiateViewController(withIdentifier: "InitialLoadViewController_Identifier") as! InitialLoadViewController
      vc.sdkInitModel=SdkInitModel(firstName: firstName, lastName: lastName, secKey: secKey, secInivialVector: secInivialVector, transUserName: transUserName, transUserPassword: transUserPassword, clientCode: clientCode, amount: amount,emailAddress: emailId,mobileNumber: mobileNumber,isProd: false,baseUrl: self.baseUrl, initiUrl: self.initUrl,transactionEnquiryUrl: self.transactionEnqUrl)
      
      
      vc.callback =  { (response:TransactionResponse)  in
        print("---------------Final Response To USER------------")
        //self.sdkResponse.text = "Response : \(String(describing: response.status))"
        
        callback([nil,response.status,response.clientTxnId]);
        vc.dismiss(animated: true)
      }
      print("yes sdk is integrated")
      let appDelegate  = UIApplication.shared.delegate as! AppDelegate
      let viewController = appDelegate.window!.rootViewController
      viewController!.present(vc, animated: true,completion: nil)

    }
  }
}
